## SimpleFS with inodes
### What
The goal of this project was to implement a simple file system using Indexed Allocation. It has been also implemented a naive interactive shell to test the filesystem.
### How
The main concepts, classes of this filesystem are: **Disk**, **Blocks** (FileBlock and DirectoryBlock), **SimpleFS**, **Shell**.
###### Disk
All the Disk functionalities are implemented in the DiskDriver class.
Disk blocks are mmaped in a file known at time time compilation.
The disk is responsable for read and write those blocks, to be able to do this there's a bitmap stored at the beginning of the disk that keeps track of the free blocks.
###### Blocks
There are two types of block: index and data.
Index blocks contain an array that store the position of the data blocks on disk.
A block can also be a file block or a directory block that store specific file or directory information.
The first file or directory block is an index block and contains a file control block, in which general information such as name and position on the disk are mantained, an array of pointers that directly point to data blocks called direct_blocks and an array of pointers that points to an index block that then points to data blocks.
In the first directory block there is also a bitmap where each bit corresponds to 
a data block entry. When a file/directory is removed from the directory the correspondent bit is set, so the correspondent data block entry can be reused later when create a new file/directory.
###### SimpleFS
In this class are implemented the filesystem functions, for example, readDir, that returns then name of all files in a directory, or createFile, that simply create a file in a given directory. There are also defined the file handle and directory handle structures used to refer to open files or directories. 
###### Shell
The shell read the input form the user and call wrappers for SimpleFS functions.
The handles returned by SimpleFS functions are stored in an array, so the user can't access them directly and use them as arguments in following calls.
In fact the user instead of passing a handler, will always pass a name of the file/directory, then the wrapper functions will get the correct handler stored in the array.
### How to run
###### build and run simple_fs with shell
- make
- ./simplefs
###### build and run simplefs test
- make simplefs_test
- ./simplefs_test 
