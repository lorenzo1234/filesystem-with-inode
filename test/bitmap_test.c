#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "bitmap.h"

#define MAX_ENTRIES 20

void print_err(char* err, char* bmap_entries, char* test_entries)
{
  printf("error: %s\n", err);
  for(int i = 0; i < MAX_ENTRIES; i++)
    printf("bmap: %d test: %d \n", bmap_entries[i], test_entries[i]);

  exit(EXIT_FAILURE);
}

void test(BitMap* bmap)
{
  time_t t;

  char test_entries[MAX_ENTRIES]; 

  memset(test_entries, 0, MAX_ENTRIES);
  srand((unsigned) time(&t));

  for(int i = 0; i < MAX_ENTRIES; i++)
    test_entries[i] = rand() & 0xff; 

  for(int i = 0; i < MAX_ENTRIES*8; i++)
  {
    int extracted_bit = ( test_entries[i/8] & (1 << ( i%8 )) ) >> ( i%8 );
    BitMap_set(bmap, i, extracted_bit);
  }

  if(memcmp(test_entries, bmap->entries, MAX_ENTRIES) != 0)
  {
    print_err("BitMap_set didn't work properly", bmap->entries, test_entries);
  }

  for(int i = 0; i < MAX_ENTRIES*8; i++)
  {
    int extracted_bit = ( test_entries[i/8] & (1 << ( i%8 )) ) >> ( i%8 );
    if(BitMap_get(bmap, i, extracted_bit) != i)
    {
      print_err("BitMap_get didnt't work properly", bmap->entries, test_entries);
    }
  }
}

int main(int agc, char** argv) 
{
  BitMap bitmap;
  char entries[MAX_ENTRIES];

  bitmap.num_bits = MAX_ENTRIES*8;
  bitmap.entries = entries;
  memset(bitmap.entries, 0x00, MAX_ENTRIES);

  test(&bitmap);

  printf("bitmap_test passed successfully.\n");

  return 0;
}
