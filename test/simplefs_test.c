#include "simplefs.h"

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <time.h>

#define NUM_BLOCKS 6*BLOCK_SIZE

#define PROFILING 1

int num_blocks;

double get_real_time_msec(void) {
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return ts.tv_sec*1E03 + ts.tv_nsec*1E-06;
}

double measure_func_time(void f(DiskDriver* disk), DiskDriver* disk)
{
  double start = get_real_time_msec();
  f(disk);
  return get_real_time_msec() - start;
}

// char** allocate_names(int n)
// {
//   char** names = malloc(n*sizeof(char*));
//   for(int i =0 ; i < n; ++i)
//   {
//     names[i] = malloc(MAX_NAME_LEN);
//   }
//   return names;
// }

// void print_names(char** names, int n)
// {
//   for(int i = 0; i < n; i++)
//   {
//     printf("i: %d, names: %s\n", i, names[i]);
//   }
// }

// void init_names(char** names, int n)
// {
//   for(int i = 0; i < n; i++)
//   {
//     strcpy(names[i], " ");
//   }
// }

// void free_names(char**names, int n)
// {
//   for(int i = 0; i < n; ++i)
//   {
//     free(names[i]);
//   }
//   free(names);
// }

void test_subdirectory(DiskDriver* disk)
{
  SimpleFS fs;

  DirectoryHandle* dhroot = SimpleFS_init(&fs, disk);

  char name[20];
  char str[11];

  // time_t t;
  // srand((unsigned) time(&t));

  int before = disk->header->free_blocks;

  char dirname[] = "dirname";
  DirectoryHandle* dh = SimpleFS_mkDir(dhroot, dirname);

  // int n  = rand()%(num_blocks/2);
  int n = num_blocks/2-64;
  for(int i = 0; i < n ; i++)
  {
    strcpy(name, "sub");
    sprintf(str, "%d", i);
    strcat(name, str);
    FileHandle* f = SimpleFS_createFile(dh, name);
    SimpleFS_close(f);
  }
  SimpleFS_remove(dhroot, dirname);
  
  int after = disk->header->free_blocks;

  if(after != before)
  {
    printf("error: free blocks lost after deleting a directory\n");
    exit(EXIT_FAILURE);
  }

  SimpleFS_close(dh);
  SimpleFS_close(dhroot);
  SimpleFS_close(SimpleFS_format(&fs));
}

void test_create_remove(DiskDriver* disk)
{
  SimpleFS fs;

  DirectoryHandle* dhroot = SimpleFS_init(&fs, disk);

  char name[20];
  char str[11];

  int ntest = num_blocks/2-64;

  for(int i = 0; i < ntest; i++)
  {
    strcpy(name, "p");
    sprintf(str, "%d", i);
    strcat(name, str);
    FileHandle* f = SimpleFS_createFile(dhroot, name);
    SimpleFS_close(f);
  }

  int before_free_blocks = disk->header->free_blocks;
  int before_num_entries = dhroot->block_handle.dcb->num_entries;

  for(int i = 0; i < ntest; i++)
  {
    strcpy(name, "p");
    sprintf(str, "%d", i);
    strcat(name, str);
    SimpleFS_remove(dhroot, name);
  }

  for(int i = 0; i < ntest; i++)
  {
    strcpy(name, "new");
    sprintf(str, "%d", i);
    strcat(name, str);
    FileHandle* f = SimpleFS_createFile(dhroot, name);
    SimpleFS_close(f);
  }

  if(before_free_blocks != disk->header->free_blocks || before_num_entries != dhroot->block_handle.dcb->num_entries)
  {
    printf("error: free blocks lost after deleting and creating %d files\n", num_blocks);
    exit(EXIT_FAILURE);
  }

  SimpleFS_close(dhroot);
  SimpleFS_close(SimpleFS_format(&fs));
}

void test_read_write(DiskDriver* disk)
{
  SimpleFS fs;

  DirectoryHandle* dhroot = SimpleFS_init(&fs, disk);

  FileHandle* fh = SimpleFS_createFile(dhroot, "prova.txt");
  SimpleFS_close(fh);
  
  fh = SimpleFS_pathResolution(&fs, "/prova.txt");
  
  char write_buffer[BLOCK_SIZE];
  char read_buffer[BLOCK_SIZE];

  char c = 32;
  for(int i = 0; i < BLOCK_SIZE; i++)
  {
    write_buffer[i] = c++;
    if(c == 127) c = 32;
  }
  
  SimpleFS_write(fh, &write_buffer, BLOCK_SIZE);
  SimpleFS_seek(fh, 0);

  SimpleFS_read(fh, &read_buffer, BLOCK_SIZE);
  SimpleFS_seek(fh, 0);

  if(memcmp(write_buffer, read_buffer, BLOCK_SIZE))
  {
    printf("error: mismatch between write and read\n");
  }

  SimpleFS_close(fh);
  SimpleFS_close(dhroot);
  SimpleFS_close(SimpleFS_format(&fs));
}

int main(int agc, char** argv) {

  DiskDriver disk;
  char *pathname = "./file_disk_driver";
  unlink(pathname);

  num_blocks = DiskDriver_init(&disk, pathname, NUM_BLOCKS);
  
#if PROFILING
  double time_subdirectory = measure_func_time(test_subdirectory, &disk);
  double time_read_write = measure_func_time(test_read_write, &disk);
  double time_create_remove = measure_func_time(test_create_remove, &disk);
  printf("test_subdirectory: %f\ntest_read_write: %f\ntest_create_remove: %f\n", time_subdirectory, time_read_write, time_create_remove);
#else
  test_subdirectory(&disk);
  test_read_write(&disk);
  test_create_remove(&disk);
#endif

  DiskDriver_free(&disk);
  DiskDriver_destroy(&disk);
 
  printf("simplefs_test passed successfully.\n");

  return 0;

}
