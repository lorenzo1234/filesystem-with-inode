#include "disk_driver.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define NUM_BLOCKS 6*BLOCK_SIZE

void print_err(char* err, int block_num)
{
  printf("error: %s\n", err);
  printf("block number:%d\n", block_num);
  exit(EXIT_FAILURE);
}

void test_write_read(DiskDriver *disk_driver)
{
  char write_buffer[BLOCK_SIZE];
  char read_buffer[BLOCK_SIZE];

  time_t t;
  srand((unsigned) time(&t));

  int block_num = rand() % NUM_BLOCKS;
  
  memset(write_buffer, 0, BLOCK_SIZE);
  
  char c = 32;
  for(int i = 0; i < BLOCK_SIZE; i++)
  {
    write_buffer[i] = c++;
    if(c == 127) c = 32;
  }

  DiskDriver_writeBlock(disk_driver, (void*)write_buffer, block_num);
  DiskDriver_readBlock(disk_driver, (void*)read_buffer, block_num);

  if(memcmp(write_buffer, read_buffer, BLOCK_SIZE))
  {
    print_err("mismatch between write and read\n", block_num);
  }
}

void test_free(DiskDriver* disk)
{
  char* test_string = "test";
  char read_buffer[BLOCK_SIZE];

  time_t t;
  srand((unsigned) time(&t));

  int block_num = rand()%(NUM_BLOCKS-1);

  for(int i = 0; i < block_num; i++)
  {
    DiskDriver_writeBlock(disk, (void*)test_string, i);  
  }
  if(DiskDriver_getFirstFreeBlock(disk) != block_num)
  {
    print_err("DiskDriver_getFirstFreeBlock() returned wrong block index", block_num);
  }
  
  for(int i = block_num; i < NUM_BLOCKS; i++)
  {
    DiskDriver_writeBlock(disk, (void*)test_string, i);
  }

  printf("attempt to get a free block even if all blocks are in use: \n\t");
  if(DiskDriver_getFirstFreeBlock(disk) != -1)
  {
    print_err("DiskDriver_getFirstFreeBlock() didn't return -1", block_num);
  }

  block_num = rand()%NUM_BLOCKS;

  DiskDriver_freeBlock(disk, block_num);

  printf("attempt to read a free block: \n\t");
  if(DiskDriver_readBlock(disk, (void*) read_buffer, block_num) == 0)
  {
    print_err("DiskDriver_readBlock() didn't return -1", block_num);
  }

  printf("\n");
}

int main(int agc, char** argv) 
{  
	DiskDriver disk;
	char *pathname = "./file_disk_driver";

  unlink(pathname);
	DiskDriver_init(&disk, pathname, NUM_BLOCKS);

  test_write_read(&disk);
  test_free(&disk);

  DiskDriver_destroy(&disk);

  printf("diskdriver_test passed successfully.\n");

  return 0;
}
