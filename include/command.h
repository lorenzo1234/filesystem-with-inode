#pragma once

#define MAX_NARGS 5
#define MAX_COMMAND_LEN 13

typedef struct s_Command
{
    char command_name[MAX_COMMAND_LEN];
    int argc;
    char* args[MAX_NARGS];
    int (*execute)(struct s_Command*);
} Command;

int CommandInit(Command* command, char* command_name, int num_arguments, int (*execute)(Command*));
int CommandsInit(void);
Command getCommand(char* command_name);

int Command_SimpleFS_init(void);
