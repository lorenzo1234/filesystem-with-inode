#pragma once

#include "block_common.h"
#include "directory_block.h"

#include <stdio.h>
#include <string.h>

/******************* stuff on disk BEGIN *******************/

// this is the first physical block of a file
// it has a header
// an FCB storing file infos
// and can contain some data
typedef struct {
  BlockHeader header;
  FileControlBlock fcb;
	int direct_blocks[(BLOCK_SIZE - sizeof(BlockHeader) - sizeof(FileControlBlock) - sizeof(int))/sizeof(int)];
  int indirect_blocks[1];
} FirstFileBlock;

// this is one of the next physical blocks of a file
typedef struct {
  BlockHeader header;
  char  data[BLOCK_SIZE-sizeof(BlockHeader)];
} FileDataBlock;

/******************* stuff on disk END *******************/

typedef struct {
  FirstFileBlock* fcb;             // pointer to the first block of the file(read it)
  BlockHeader* current_block;      // current block in the file (it must be a data block)
  int pos_in_file;                 // position of the cursor (in byte)
  int current_block_idx;           // block index relative to the file (first block data: 0, second block data: 1 and so on)
} FileBlockHandle;

// write a FirstFileBlock and also a FileDataBlock, saved in direct_blocks[0], in disk
// return position of the FirstFileBlock on the disk
int FirstFileBlock_init(DiskDriver* disk, const char* filename, int directory_block);

// return position of the FileDataBlock on the disk
int FileDataBlock_init(DiskDriver* disk, int free_block);

void FileBlockHandle_init(FileBlockHandle* fbh,  FirstFileBlock* ffb, FileDataBlock* fdb);

int FileBlock_write(DiskDriver* disk, FileBlockHandle* fbh, void* data, int size);

int FileBlock_read(DiskDriver* disk, FileBlockHandle* fbh, void* data, int size);

int FileBlock_destroy(DiskDriver* disk, FirstFileBlock* ffb);

int FileBlock_seek(DiskDriver* disk, FileBlockHandle* fbh, int pos);