#pragma once

#include "file_block.h"
#include "directory_block.h"

#define SIMPLEFS_MAXLEN_PATH 20

typedef struct {
  DiskDriver* disk;
  char current_dir[SIMPLEFS_MAXLEN_PATH];
  // add more fields if needed
} SimpleFS;

// this is a file handle, used to refer to open files
typedef struct {
  SimpleFS* sfs;                   // pointer to memory file system structure
  FileBlockHandle block_handle;
} FileHandle;

typedef struct {
  SimpleFS* sfs;                   // pointer to memory file system structure
  DirectoryBlockHandle block_handle;
} DirectoryHandle;

// initializes a file system on an already made disk
// returns a handle to the top level directory stored in the first block
DirectoryHandle* SimpleFS_init(SimpleFS* fs, DiskDriver* disk);

// creates the inital structures, the top level directory
// has name "/" and its control block is in the first position
// it also clears the bitmap of occupied blocks on the disk
// the current_directory_block is cached in the SimpleFS struct
// and set to the top level directory
DirectoryHandle* SimpleFS_format(SimpleFS* fs);

// creates an empty file in the directory d
// returns null on error (file existing, no free blocks)
// an empty file consists only of a block of type FirstBlock
FileHandle* SimpleFS_createFile(DirectoryHandle* d, const char* filename);

// reads in the (preallocated) blocks array, the name of all files in a directory 
int SimpleFS_readDir(char** names, DirectoryHandle* d);

// closes a file handle (destroyes it)
int SimpleFS_close(void* f);

// writes in the file, at current position for size bytes stored in data
// overwriting and allocating new space if necessary
// returns the number of bytes written
int SimpleFS_write(FileHandle* f, void* data, int size);

// writes in the file, at current position size bytes stored in data
// overwriting and allocating new space if necessary
// returns the number of bytes read
int SimpleFS_read(FileHandle* f, void* data, int size);

// returns the number of bytes read (moving the current pointer to pos)
// returns pos on success
// -1 on error (file too short)
int SimpleFS_seek(FileHandle* f, int pos);

// creates a new directory in the directory dhparent
// -1 on error
DirectoryHandle* SimpleFS_mkDir(DirectoryHandle* dhparent, char* dirname);

// removes the file in the directory dhparent
// returns -1 on failure 0 on success
// if a directory, it removes recursively all contained files
int SimpleFS_remove(DirectoryHandle* dhparent, char* filename);

void* SimpleFS_pathResolution(SimpleFS* fs, char* path);

void SimpleFS_relativeToAbsolutePath(SimpleFS* fs, char* abs, char* rel);

int SimpleFS_extractLastComponent(char* last_component, char* path);

DirectoryHandle* DirectoryHandle_init(SimpleFS* fs, FirstDirectoryBlock* fdb, DirectoryDataBlock* ddb);

FileHandle* FileHandle_init(DirectoryHandle* d, FirstFileBlock* ffb, FileDataBlock* fdb);