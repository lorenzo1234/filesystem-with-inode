#pragma once

#include "simplefs.h"

#define RUNTIMEMANAGER_NBLOCKS 6*BLOCK_SIZE

#define RUNTIMEMANAGER_MAX_MOUNT_FS 2

int RunTimeManager_init(void);

int RunTimeManager_format(char* diskname);

// seeks for a directory in path.
// 0 on success, negative value on error
int RunTimeManager_changeDir(char* path);

void* RunTimeManager_pathResolution(char* path);

int RunTimeManager_mount(char* mounting_point, char* diskname, int num_blocks);
int RunTimeManager_unmount(char* mounting_point);
void RunTimeManager_resetMountTable(void);

int RunTimeManager_addFileHandle(FileHandle* h, char* path);
void RunTimeManager_removeFileHandle(FileHandle* h);
FileHandle* RunTimeManager_getFileHandleByPath(char* path);
void RunTimeManager_resetOpenFileTable(void);

int RunTimeManager_addDirHandle(DirectoryHandle* dh, char* path);
void RunTimeManager_removeDirHandleByPath(DirectoryHandle* dh);
// path must be already sanitized
DirectoryHandle* RunTimeManager_getDirHandleByPath(char* path);
void RunTimeManager_resetDirCache(void);

int RunTimeManager_listDisk(char** names);

SimpleFS* RunTimeManager_getSimpleFS(void);

int RunTimeManager_running(void);
void RunTimeManager_exit(void);

void print_open_file_table(void);
void print_mount_table(void);
void print_dir_cache(void);
