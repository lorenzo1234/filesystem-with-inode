#pragma once

#include "disk_driver.h"

#define MAX_NAME_LEN 128
#define BLOCKCOMMON_FREE -1

/*these are structures stored on disk*/

// header, occupies the first portion of each block in the disk
// represents a chained list of blocks
typedef struct {
  int block_in_file; // position in the file, if 0 we have a file control block
  int is_data_block; // 0 for data, 1 for index
} BlockHeader;

// this is in the first block of a chain, after the header
typedef struct {
  int directory_block; // first block of the parent directory
  int block_in_disk;   // repeated position of the block on the disk
  char name[MAX_NAME_LEN];
  int  size_in_bytes;
  int size_in_blocks;
  int is_dir;          // 0 for file, 1 for dir
} FileControlBlock;

typedef struct
{
	BlockHeader header;
	int next_blocks[(BLOCK_SIZE - sizeof(BlockHeader))/sizeof(int)];
} IndexBlock;

int IndexBlock_init(DiskDriver* disk, int free_block);

// return the first array index such that array[index] = BLOCKCOMMON_FREE
int BlockCommon_getFreeBlock(int array[], int size);

void BlockCommon_fillArray(int *array, int c, int len);