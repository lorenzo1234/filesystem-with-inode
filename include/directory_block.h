#pragma once

#include "block_common.h"

/******************* stuff on disk BEGIN *******************/

#define NUM_DIRECT_BLOCKS 1
#define NUM_INDIRECT_BLOCKS 1

// this is the first physical block of a directory
typedef struct {
  BlockHeader header;
  FileControlBlock fcb;
  int num_entries;
  char bitmap_data[BLOCK_SIZE
  -sizeof(BlockHeader)
  -sizeof(FileControlBlock)
  -sizeof(int)
  -NUM_DIRECT_BLOCKS*sizeof(int)
  -NUM_INDIRECT_BLOCKS*sizeof(int)
  ];
  int direct_blocks[NUM_DIRECT_BLOCKS];
  int indirect_blocks[NUM_INDIRECT_BLOCKS];
} FirstDirectoryBlock;

// this is remainder block of a directory
typedef struct {
  BlockHeader header;
  int file_blocks[ (BLOCK_SIZE-sizeof(BlockHeader))/sizeof(int) ];
} DirectoryDataBlock;

/******************* stuff on disk END *******************/
typedef struct {
  FirstDirectoryBlock* dcb;        // pointer to the first block of the directory(read it)
  BlockHeader* current_block;      // current block in the directory (it must be a data block)
  int pos_in_dir;                  // absolute position of the cursor in the directory
  int pos_in_block;                // relative position of the cursor in the block
} DirectoryBlockHandle;

// write a FirstDirectoryBlock and also a DirectoryDataBlock, saved in direct_blocks[0], in disk
// return position of the FirstDirectoryBlock on the disk
int FirstDirectoryBlock_init(DiskDriver* disk, char* dirname, int parent_block);

// return position of the DirectoryDataBlock on the disk
int DirectoryDataBlock_init(DiskDriver* disk, int free_block);

void DirectoryBlockHandle_init(DirectoryBlockHandle* dbh, FirstDirectoryBlock* fdb, DirectoryDataBlock* ddb);

// add new file block in FirstDirectoryBlock pointed by dbh
int DirectoryBlock_addFile(DiskDriver* disk, DirectoryBlockHandle* dbh, int block_file);

// return a block saved in fdb
// file_idx is the position of the block in fdb
int DirectoryBlock_getFile(DiskDriver* disk, FirstDirectoryBlock* fdb, int file_idx);

// search for a block with given filename in FirstDirectoryBlock pointed by dbh
// return position of the block on the disk
int DirectoryBlock_searchFile(DiskDriver* disk, DirectoryBlockHandle* dbh, const char* filename, int recursive);

// remove a block with given filename in FirstDirectoryBlock pointed by dbh
// return 0 on success or -1 on failure
int DirectoryBlock_removeFile(DiskDriver* disk, DirectoryBlockHandle* dbh, const char* filename);

int DirectoryBlock_destroy(DiskDriver* disk, FirstDirectoryBlock* fdb);

int DirectoryDataBlock_destroy(DiskDriver* disk, DirectoryDataBlock* ddb);