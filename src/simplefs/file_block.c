#include "file_block.h"

#include <stdlib.h>
#include <assert.h>

#define MEMBER_SIZE(type, member) sizeof(((type *)0)->member)

#define INDIRECT_BLOCK_SZ (MEMBER_SIZE(FirstFileBlock, indirect_blocks)/sizeof(int))
#define DIRECT_BLOCK_SZ (MEMBER_SIZE(FirstFileBlock, direct_blocks)/sizeof(int))
#define INDEX_BLOCK_SZ (MEMBER_SIZE(IndexBlock, next_blocks)/sizeof(int))
#define DATA_SZ MEMBER_SIZE(FileDataBlock, data)

/******************************************************/
/*******************PRIVATE FUNCTIONS******************/
/******************************************************/

// if file_idx is in direct_block range return 1 else 0
// i  are index that can be used to index direct_blocks 
static int _inDirectBlockRange(int file_idx, int* i)
{
    int direct_block_sz = DIRECT_BLOCK_SZ;
    int direct_nblocks = direct_block_sz;

    *i = file_idx;

    return file_idx < direct_nblocks;
}

// if file_idx is in indirect_block range return 1 else 0
// i, j are index that can be used to index indirect_blocks 
static int _inIndirectBlockIndexes(int file_idx, int* i, int* j) 
{

    int direct_block_sz = DIRECT_BLOCK_SZ;
    int direct_nblocks = direct_block_sz;

    file_idx -= direct_nblocks;

    int indirect_block_sz = INDIRECT_BLOCK_SZ;
    int index_blocks_sz = INDEX_BLOCK_SZ;

    int indirect_nblocks = indirect_block_sz*index_blocks_sz;

    *i = file_idx / (index_blocks_sz);
    *j = (file_idx - (*i)*index_blocks_sz);

    return file_idx < indirect_nblocks;
}

// add a new block to data_blocks array
// if is_direct 0 add a new IndexBlock
// else add a new FileDataBlock
static int FileIndexBlock_addBlock(DiskDriver* disk, int* data_blocks, int idx_block, int is_direct)
{
    int new_block;
    if(is_direct)
        new_block = FileDataBlock_init(disk, DiskDriver_getFirstFreeBlock(disk));
    else
        new_block = IndexBlock_init(disk, DiskDriver_getFirstFreeBlock(disk));

    data_blocks[idx_block] = new_block;
    return new_block;   
}

// add a new FileDataBlock and return its position on disk
static int FileBlock_addDataBlock(DiskDriver* disk, FirstFileBlock* ffb, int last_idx)
{    
    int i, j;
    if(_inDirectBlockRange(last_idx, &i))
    {        
        int new_ddb_block = FileIndexBlock_addBlock(disk, ffb->direct_blocks, i, 1);
        DiskDriver_writeBlock(disk, ffb, ffb->fcb.block_in_disk);

        return new_ddb_block;
    }
    if(_inIndirectBlockIndexes(last_idx, &i, &j))
    {
        if(ffb->indirect_blocks[i] == BLOCKCOMMON_FREE)
        {
            FileIndexBlock_addBlock(disk, ffb->indirect_blocks, i, 0);
        }
        IndexBlock ib;
        DiskDriver_readBlock(disk, &ib, ffb->indirect_blocks[i]);
        int new_ddb_block = FileIndexBlock_addBlock(disk, ib.next_blocks, j, 1);
        DiskDriver_writeBlock(disk, &ib, ffb->indirect_blocks[i]);
        DiskDriver_writeBlock(disk, ffb, ffb->fcb.block_in_disk);
        
        return new_ddb_block;
    }

    return -1;
}

// return a block saved in ffb
// block_idx is the position of the block in ffb
static int FileBlock_getDataBlock(DiskDriver* disk, FirstFileBlock* ffb, int block_idx)
{
    int i, j;

    if(_inDirectBlockRange(block_idx, &i))
    {
        if(ffb->direct_blocks[i] == BLOCKCOMMON_FREE)
            goto not_found;
        
        return ffb->direct_blocks[i];
    }
    IndexBlock ib;
    if(_inIndirectBlockIndexes(block_idx, &i, &j))
    {
        if(ffb->indirect_blocks[i] == BLOCKCOMMON_FREE)
            goto not_found; 

        DiskDriver_readBlock(disk, &ib, ffb->indirect_blocks[i]);
        if(ib.next_blocks[j] == BLOCKCOMMON_FREE)
            goto not_found;

        return ib.next_blocks[j];
    }

not_found:
    return -1;
}

static int FileBlock_rw(DiskDriver* disk, FileBlockHandle* fbh, void* data, int size, const int writing)
{
    FirstFileBlock ffb;
    FileDataBlock fdb;
    
    int offset = fbh->pos_in_file;
    int sz_to_copy = size+offset < DATA_SZ ? size : DATA_SZ-offset;
    int curr_data_block = fbh->current_block->block_in_file;

    DiskDriver_readBlock(disk, &ffb, fbh->fcb->fcb.block_in_disk);

    // first copy
    DiskDriver_readBlock(disk, &fdb, curr_data_block);
    
    if(writing) 
        memcpy(fdb.data + offset, data, sz_to_copy);
    else 
        memcpy(data, fdb.data + offset, sz_to_copy);
    
    DiskDriver_writeBlock(disk, &fdb, curr_data_block);

    int data_idx = sz_to_copy;
    size -= sz_to_copy;

    while(size > 0)
    {
        fbh->current_block_idx++;    

        sz_to_copy = size >= DATA_SZ ? DATA_SZ : size; 

        curr_data_block = FileBlock_getDataBlock(disk, &ffb, fbh->current_block_idx);
        if(curr_data_block == -1)
        {
            if(writing)
                curr_data_block = FileBlock_addDataBlock(disk, &ffb, fbh->current_block_idx);
            else
                return -1;            
        }
        DiskDriver_readBlock(disk, &fdb, curr_data_block);
        if(writing)
            memcpy(fdb.data, data+data_idx, sz_to_copy);
        else
            memcpy(data+data_idx, fdb.data, sz_to_copy);

        DiskDriver_writeBlock(disk, &fdb, curr_data_block);

        data_idx += sz_to_copy;
        size -= sz_to_copy;

    }

    fbh->pos_in_file = sz_to_copy;
    fbh->current_block = (BlockHeader*) DiskDriver_getBlock(disk, curr_data_block);

    return 0;
}
/******************************************************/
/******************************************************/
/******************************************************/


int FirstFileBlock_init(DiskDriver* disk, const char* filename, int directory_block)
{
    FirstFileBlock ffb = {
        .fcb.size_in_blocks = 1,
        .fcb.directory_block = directory_block
    };

    if(strlen(filename) >= MAX_NAME_LEN)
    {
        printf("FirstFileBlock_init(): filename length greater than the allowed");
        return -1;
    }

    ffb.fcb.block_in_disk = DiskDriver_getFirstFreeBlock(disk);
    
    BlockCommon_fillArray(ffb.direct_blocks, BLOCKCOMMON_FREE, sizeof(ffb.direct_blocks)/sizeof(int));
    BlockCommon_fillArray(ffb.indirect_blocks, BLOCKCOMMON_FREE, sizeof(ffb.indirect_blocks)/sizeof(int));
    
    strcpy(ffb.fcb.name, filename);

    int free_block = DiskDriver_getFreeBlock(disk, ffb.fcb.block_in_disk+1);
    ffb.direct_blocks[0] = FileDataBlock_init(disk, free_block);
    ffb.fcb.size_in_blocks++;

    int res = DiskDriver_writeBlock(disk, (void*) &ffb, ffb.fcb.block_in_disk);
    if(res == -1)
    {
        printf("DiskDriver_writeBlock() return an error\n");
        exit(EXIT_FAILURE);
    }

    return ffb.fcb.block_in_disk;
}

int FileDataBlock_init(DiskDriver* disk, int free_block)
{
    FileDataBlock fdb = {
        .header.is_data_block = 1
    };
    fdb.header.block_in_file = free_block;

    memset(fdb.data, 0, sizeof(fdb.data)/sizeof(int));

    DiskDriver_writeBlock(disk, &fdb, fdb.header.block_in_file);

    return fdb.header.block_in_file;
}

void FileBlockHandle_init(FileBlockHandle* fbh,  FirstFileBlock* ffb, FileDataBlock* fdb)
{
    fbh->fcb = ffb;
    fbh->current_block = &fdb->header;
    fbh->pos_in_file = 0;
    fbh->current_block_idx = 0;
}

int FileBlock_write(DiskDriver* disk, FileBlockHandle* fbh, void* data, int size)
{
    return FileBlock_rw(disk, fbh, data, size, 1);
}

int FileBlock_read(DiskDriver* disk, FileBlockHandle* fbh, void* data, int size)
{
    return FileBlock_rw(disk, fbh, data, size, 0);
}

int FileBlock_seek(DiskDriver* disk, FileBlockHandle* fbh, int pos)
{
    if(pos < 0)
    {
        printf("position must be greater or euqal than 0\n");
        return -1;
    }

    FirstFileBlock ffb;
    FileDataBlock fdb;

    fbh->pos_in_file = 0;
    fbh->current_block_idx = 0;
    DiskDriver_readBlock(disk, &ffb, fbh->fcb->fcb.block_in_disk);
    
    int data_block = FileBlock_getDataBlock(disk, &ffb, fbh->current_block_idx);
    assert(data_block != -1);

    while(pos >= DATA_SZ)
    {
        data_block = FileBlock_getDataBlock(disk, &ffb, fbh->current_block_idx);
        if(data_block == -1)
        {
            return -1;
        }
        DiskDriver_readBlock(disk, &fdb, data_block);
        fbh->current_block_idx++;
        pos -= DATA_SZ;
    }

    fbh->pos_in_file = pos;
    fbh->current_block = (BlockHeader*) DiskDriver_getBlock(disk, data_block);

    return fbh->pos_in_file;
}

int FileBlock_destroy(DiskDriver* disk, FirstFileBlock* ffb)
{
    for(int i = 0; i < DIRECT_BLOCK_SZ; ++i)
    {
        FileDataBlock fdb;

        if(ffb->direct_blocks[i] == BLOCKCOMMON_FREE)
            continue;

        DiskDriver_readBlock(disk, &fdb, ffb->direct_blocks[i]);
        DiskDriver_freeBlock(disk, ffb->direct_blocks[i]);
    }
    for(int i = 0; i < INDIRECT_BLOCK_SZ; ++i)
    {
        IndexBlock ib;

        if(ffb->indirect_blocks[i] == BLOCKCOMMON_FREE)
            continue;

        DiskDriver_readBlock(disk, &ib, ffb->indirect_blocks[i]);
        for(int j = 0; j < INDEX_BLOCK_SZ; ++j)
        {
            FileDataBlock fdb;

            if(ib.next_blocks[j] == BLOCKCOMMON_FREE)
                continue;

            DiskDriver_readBlock(disk, &fdb, ib.next_blocks[j]);
            DiskDriver_freeBlock(disk, ib.next_blocks[j]);
        }
        DiskDriver_freeBlock(disk, ffb->indirect_blocks[i]);
    }
    DiskDriver_freeBlock(disk, ffb->fcb.block_in_disk);

    return 0;
}