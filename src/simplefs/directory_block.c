#include "directory_block.h"
#include "file_block.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define MEMBER_SIZE(type, member) (sizeof(((type *)0)->member))

#define INDIRECT_BLOCK_SZ (MEMBER_SIZE(FirstDirectoryBlock, indirect_blocks)/sizeof(int))
#define DIRECT_BLOCK_SZ (MEMBER_SIZE(FirstDirectoryBlock, direct_blocks)/sizeof(int))
#define INDEX_BLOCK_SZ (MEMBER_SIZE(IndexBlock, next_blocks)/sizeof(int))
#define FILE_BLOCK_SZ (MEMBER_SIZE(DirectoryDataBlock, file_blocks)/sizeof(int))

#define BITMAP_SZ ((MEMBER_SIZE(FirstDirectoryBlock, bitmap_data))*8)

#define BITMAP_FREE 0 // previously used and now available to be reused
#define BITMAP_INUSE 1 // not allocated yet or currently in use

/******************************************************/
/*******************PRIVATE FUNCTIONS******************/
/******************************************************/

// if file_idx is in direct_block range return 1 else 0
// i, j are index that can be used to index direct_blocks 
static int _inDirectBlockRange(int file_idx, int* i, int* j)
{
    int direct_block_sz = DIRECT_BLOCK_SZ;
    int file_block_sz = FILE_BLOCK_SZ;
    int direct_nblocks = direct_block_sz*file_block_sz;

    *i = file_idx / file_block_sz;
    *j = file_idx - (*i)*file_block_sz;

    return file_idx < direct_nblocks;
}

// if file_idx is in indirect_block range return 1 else 0
// k, i, j are index that can be used to index indirect_blocks 
static int _inIndirectBlockIndexes(int file_idx, int* k, int* i, int* j) 
{
    // lin_idx = k*row*col + i*col + j
    // k = (lin_idx                    ) // (row*col)
    // i = (lin_idx - k*row*col        ) // (col)
    // j = (lin_idx - k*row*col - i*col) // 1

    int direct_block_sz = DIRECT_BLOCK_SZ;
    int file_block_sz = FILE_BLOCK_SZ;
    int direct_nblocks = direct_block_sz*file_block_sz;

    file_idx -= direct_nblocks;

    int indirect_block_sz = INDIRECT_BLOCK_SZ;
    int index_blocks_sz = INDEX_BLOCK_SZ;

    int indirect_nblocks = indirect_block_sz*index_blocks_sz*file_block_sz;

    *k = file_idx / (index_blocks_sz*file_block_sz);
    *i = (file_idx - (*k)*index_blocks_sz*file_block_sz) / file_block_sz;
    *j = (file_idx - (*k)*index_blocks_sz*file_block_sz - (*i)*file_block_sz);

    return file_idx < indirect_nblocks;
}

// add a new block to data_blocks array
// if is_direct 0 add a new IndexBlock else add a new DirectoryDataBlock
static int DirectoryIndexBlock_addBlock(DiskDriver* disk, int* data_blocks, int idx_block, int is_direct)
{
    int new_block;
    if(is_direct)
        new_block = DirectoryDataBlock_init(disk, DiskDriver_getFirstFreeBlock(disk));
    else
        new_block = IndexBlock_init(disk, DiskDriver_getFirstFreeBlock(disk));

    data_blocks[idx_block] = new_block;
    return new_block;
}

// return a DirectoryDataBlock that has a free block
static int DirectoryBlock_getFreeBlock(DiskDriver* disk, FirstDirectoryBlock* fdb)
{
    BitMap bmap;
    bmap.entries = fdb->bitmap_data;
    bmap.num_bits = BITMAP_SZ;

    int ddb_idx = BitMap_get(&bmap, 0, BITMAP_FREE);
    if(ddb_idx == -1)
    {
        return -1;
    }
    BitMap_set(&bmap, ddb_idx, BITMAP_INUSE);
    
    int k, i, j;

    if(_inDirectBlockRange(ddb_idx, &i, &j))
    {
        if(fdb->direct_blocks[i] == BLOCKCOMMON_FREE) 
            return -1;
    
        DiskDriver_writeBlock(disk, fdb, fdb->header.block_in_file);
        return fdb->direct_blocks[i];
    }
    IndexBlock ib;
    if(_inIndirectBlockIndexes(ddb_idx, &k, &i, &j))
    {
        if(fdb->indirect_blocks[k] == BLOCKCOMMON_FREE)
            return -1;

        DiskDriver_readBlock(disk, &ib, fdb->indirect_blocks[k]);
        if(ib.next_blocks[i] == BLOCKCOMMON_FREE)
            return -1;
                
        DiskDriver_writeBlock(disk, fdb, fdb->header.block_in_file);
        return ib.next_blocks[i];
    }

    return -1;
}

// mark the block as reusable
static int DirectoryBlock_addFreeBlock(DiskDriver* disk, FirstDirectoryBlock* fdb, int target)
{
    BitMap bmap;
    bmap.entries = fdb->bitmap_data;
    bmap.num_bits = BITMAP_SZ;

    BitMap_set(&bmap, target, BITMAP_FREE);
    DiskDriver_writeBlock(disk, fdb, fdb->fcb.block_in_disk);
    
    return 0;
}

// add a new DirectoryDataBlock and return its position on disk
static int DirectoryIndexBlock_addDataBlock(DiskDriver* disk, FirstDirectoryBlock* fdb)
{
    int last_idx = fdb->num_entries;
    
    int k, i, j;
    if(_inDirectBlockRange(last_idx, &i, &j))
    {        
        return DirectoryIndexBlock_addBlock(disk, fdb->direct_blocks, i, 1);
    }
    if(_inIndirectBlockIndexes(last_idx, &k, &i, &j))
    {
        if(fdb->indirect_blocks[k] == BLOCKCOMMON_FREE)
        {
            DirectoryIndexBlock_addBlock(disk, fdb->indirect_blocks, k, 0);
        }
        IndexBlock ib;
        DiskDriver_readBlock(disk, &ib, fdb->indirect_blocks[k]);
        int new_ddb_block = DirectoryIndexBlock_addBlock(disk, ib.next_blocks, i, 1);
        DiskDriver_writeBlock(disk, &ib, fdb->indirect_blocks[k]);
        return new_ddb_block;
    }

    return -1;
}

static int DirectoryDataBlock_removeBlock(DiskDriver* disk, FirstDirectoryBlock* fdb, int file_idx)
{
    int k, i, target;

    DirectoryDataBlock ddb;
    if(_inDirectBlockRange(file_idx, &i, &target))
    {
        if(fdb->direct_blocks[i] == BLOCKCOMMON_FREE) 
            goto not_found;

        DiskDriver_readBlock(disk, &ddb, fdb->direct_blocks[i]);
        if(ddb.file_blocks[target] == BLOCKCOMMON_FREE)
            goto not_found;

        goto found;
    }
    IndexBlock ib;
    if(_inIndirectBlockIndexes(file_idx, &k, &i, &target))
    {
        if(fdb->indirect_blocks[k] == BLOCKCOMMON_FREE)
        goto not_found;

        DiskDriver_readBlock(disk, &ib, fdb->indirect_blocks[k]);
        if(ib.next_blocks[i] == BLOCKCOMMON_FREE)
            goto not_found;
        
        DiskDriver_readBlock(disk, &ddb, ib.next_blocks[i]);
        if(ddb.file_blocks[target] == BLOCKCOMMON_FREE)
            goto not_found;

        goto found;
    }

not_found:
    printf("DirectoryDataBlock_removeFile(): Directory %s does't contain DataBlock in %d\n", fdb->fcb.name, file_idx);
    return -1;

found:
    ;

    FirstDirectoryBlock* pfdb;

    pfdb = (FirstDirectoryBlock*) DiskDriver_getBlock(disk, ddb.file_blocks[target]);
    if(pfdb->fcb.is_dir)
    {
        FirstDirectoryBlock target_fdb;
        DiskDriver_readBlock(disk, &target_fdb, ddb.file_blocks[target]);
        DirectoryBlock_destroy(disk, &target_fdb);
    }
    else
    {
        FirstFileBlock target_ffb;
        DiskDriver_readBlock(disk, &target_ffb, ddb.file_blocks[target]);
        FileBlock_destroy(disk, &target_ffb);
    }
    DirectoryBlock_addFreeBlock(disk, fdb, file_idx);
    ddb.file_blocks[target] = BLOCKCOMMON_FREE;
    DiskDriver_writeBlock(disk, &ddb, ddb.header.block_in_file);
    return 0;
}
/******************************************************/
/******************************************************/
/******************************************************/

int FirstDirectoryBlock_init( DiskDriver* disk, char* dirname, int parent_block)
{
    FirstDirectoryBlock fdb = {
        .fcb.size_in_blocks = 1,
        .fcb.is_dir = 1,
        .fcb.directory_block = parent_block
    };

    if(strlen(dirname) >= MAX_NAME_LEN)
    {
        printf("FirstDirectoryBlock_init(): dirname length greater than the allowed");
        return -1;
    }

    fdb.fcb.block_in_disk = DiskDriver_getFirstFreeBlock(disk); 

    BlockCommon_fillArray(fdb.direct_blocks, BLOCKCOMMON_FREE, sizeof(fdb.direct_blocks)/sizeof(int));
    BlockCommon_fillArray(fdb.indirect_blocks, BLOCKCOMMON_FREE, sizeof(fdb.indirect_blocks)/sizeof(int));
    
    strcpy(fdb.fcb.name, dirname);

    int free_block = DiskDriver_getFreeBlock(disk, fdb.fcb.block_in_disk+1);
    fdb.direct_blocks[0] = DirectoryDataBlock_init(disk, free_block);
    fdb.fcb.size_in_blocks++;

    BitMap bmap;
    bmap.entries = fdb.bitmap_data;
    bmap.num_bits = BITMAP_SZ;
    for(int i = 0; i < bmap.num_bits; ++i)
        BitMap_set(&bmap, i, BITMAP_INUSE);

    int res = DiskDriver_writeBlock(disk, (void*) &fdb, fdb.fcb.block_in_disk);
    if(res == -1)
    {
        printf("DiskDriver_writeBlock() return an error\n");
        exit(EXIT_FAILURE);
    }

    return fdb.fcb.block_in_disk;
}

int DirectoryDataBlock_init(DiskDriver* disk, int free_block)
{
    DirectoryDataBlock ddb = {
        .header.is_data_block = 1
    };
    ddb.header.block_in_file = free_block;
    BlockCommon_fillArray(ddb.file_blocks, BLOCKCOMMON_FREE, sizeof(ddb.file_blocks)/sizeof(int));

    DiskDriver_writeBlock(disk, &ddb, ddb.header.block_in_file);

    return ddb.header.block_in_file;
}

int DirectoryBlock_addFile(DiskDriver* disk, DirectoryBlockHandle* dbh, int block_file)
{
    FirstDirectoryBlock fdb;
    DirectoryDataBlock ddb;

    int first_dir_block = dbh->dcb->fcb.block_in_disk;

    DiskDriver_readBlock(disk, &fdb, first_dir_block);

    int new_block = 0;
    int dir_data_block = DirectoryBlock_getFreeBlock(disk, &fdb);
    if(dir_data_block == -1)
    {
        if(fdb.num_entries > BITMAP_SZ)
        {
            printf("DirectoryBlock_addFile(): directory %s has too many entries\n", fdb.fcb.name);
            return -1;
        }
        
        dir_data_block = dbh->current_block->block_in_file;
        new_block = 1; 
    }

    DiskDriver_readBlock(disk, &ddb, dir_data_block);

    int free_location = BlockCommon_getFreeBlock(ddb.file_blocks, FILE_BLOCK_SZ);
    if(free_location == -1)
    {
        dir_data_block = DirectoryIndexBlock_addDataBlock(disk, &fdb);
        
        DiskDriver_readBlock(disk, &ddb, dir_data_block);
        dbh->current_block = (BlockHeader*) DiskDriver_getBlock(disk, dir_data_block);
        free_location = BlockCommon_getFreeBlock(ddb.file_blocks, FILE_BLOCK_SZ);

        fdb.fcb.size_in_blocks++;
        dbh->pos_in_block = 0;
        dbh->pos_in_dir++;
    }

    ddb.file_blocks[free_location] = block_file;
   
    fdb.fcb.size_in_bytes += sizeof(int);
    dbh->pos_in_block++;
    
    if(new_block) 
        fdb.num_entries++; // if we use a previously freed block we don't incrment num_entries

    DiskDriver_writeBlock(disk, &fdb, first_dir_block);
    DiskDriver_writeBlock(disk, &ddb, dir_data_block);

    return 0;
}

void DirectoryBlockHandle_init(DirectoryBlockHandle* dbh, FirstDirectoryBlock* fdb, DirectoryDataBlock* ddb)
{
    dbh->dcb = fdb;
    dbh->current_block = &ddb->header;
    dbh->pos_in_dir = 0;
    dbh->pos_in_block = 0;
}

int DirectoryBlock_getFile(DiskDriver* disk, FirstDirectoryBlock* fdb, int file_idx)
{
    int k, i, j;

    DirectoryDataBlock ddb;

    if(_inDirectBlockRange(file_idx, &i, &j))
    {
        if(fdb->direct_blocks[i] == BLOCKCOMMON_FREE) 
        goto not_found;

        DiskDriver_readBlock(disk, &ddb, fdb->direct_blocks[i]);
        if(ddb.file_blocks[j] == BLOCKCOMMON_FREE)
            goto not_found;

        return ddb.file_blocks[j];
    }
    IndexBlock ib;
    if(_inIndirectBlockIndexes(file_idx, &k, &i, &j))
    {
        if(fdb->indirect_blocks[k] == BLOCKCOMMON_FREE)
            goto not_found;

        DiskDriver_readBlock(disk, &ib, fdb->indirect_blocks[k]);
        if(ib.next_blocks[i] == BLOCKCOMMON_FREE)
            goto not_found;
        
        DiskDriver_readBlock(disk, &ddb, ib.next_blocks[i]);
        if(ddb.file_blocks[j] == BLOCKCOMMON_FREE)
            goto not_found;

        return ddb.file_blocks[j];
    }

not_found:
    // printf("DirectoryDataBlock_getFile(): Directory %s doesn't contain DataBlock in %d\n", fdb->fcb.name, file_idx);
    return -1;
}

int DirectoryBlock_searchFile(DiskDriver* disk, DirectoryBlockHandle* dbh, const char* filename, int recursive) // consider remove possibility to do recursive search
{
    if(strlen(filename) >= MAX_NAME_LEN)
    {
        printf("DirectoryBlock_searchFile(): filename length greater than the allowed");
        return -1;
    }

    FirstDirectoryBlock fdb;
    DiskDriver_readBlock(disk, &fdb, dbh->dcb->fcb.block_in_disk);

    for(int i = 0; i < dbh->dcb->num_entries; ++i)
    {
        int file_block = DirectoryBlock_getFile(disk, &fdb, i);
        if(file_block == -1)
        {
            continue;
        }
        FirstDirectoryBlock* pfdb = (FirstDirectoryBlock*) DiskDriver_getBlock(disk, file_block);
        if(strcmp(filename, pfdb->fcb.name) == 0)
        {
            return file_block;
        }
        else if(pfdb->fcb.is_dir && recursive)
        {
            DirectoryBlockHandle sub_dbh;
            FirstDirectoryBlock sub_fdb;
            DirectoryDataBlock sub_ddb;

            DiskDriver_readBlock(disk, &sub_fdb, file_block);
            DiskDriver_readBlock(disk, &sub_ddb, fdb.direct_blocks[0]);
            DirectoryBlockHandle_init(&sub_dbh, &sub_fdb, &sub_ddb);
            int ret = DirectoryBlock_searchFile(disk, &sub_dbh, filename, recursive);
            if(ret >= 0)
            {
                return ret;
            }
        }
    }

    return -1;
}

int DirectoryBlock_removeFile(DiskDriver* disk, DirectoryBlockHandle* dbh, const char* filename)
{
    if(strlen(filename) >= MAX_NAME_LEN)
    {
        printf("DirectoryBlock_removeFile(): filename length greater than the allowed");
        return -1;
    }

    FirstDirectoryBlock fdb;
    DiskDriver_readBlock(disk, &fdb, dbh->dcb->fcb.block_in_disk);

    for(int i = 0; i < dbh->dcb->num_entries; ++i)
    {
        int file_block = DirectoryBlock_getFile(disk, &fdb, i);
        if(file_block == -1)
        {
            continue;
        }
        FirstDirectoryBlock* pfdb = (FirstDirectoryBlock*) DiskDriver_getBlock(disk, file_block);
        if(strcmp(filename, pfdb->fcb.name) == 0)
        {
            DirectoryDataBlock_removeBlock(disk, &fdb, i);
            return 0;
        }
        else if(pfdb->fcb.is_dir)
        {
            DirectoryBlockHandle sub_dbh;
            FirstDirectoryBlock sub_fdb;
            DirectoryDataBlock sub_ddb;

            DiskDriver_readBlock(disk, &sub_fdb, file_block);
            DiskDriver_readBlock(disk, &sub_ddb, fdb.direct_blocks[0]);
            DirectoryBlockHandle_init(&sub_dbh, &sub_fdb, &sub_ddb);
            int ret = DirectoryBlock_removeFile(disk, &sub_dbh, filename);
            if(ret == 0)
            {
                return 0;
            }
        }
    }

    return -1;
}


int DirectoryDataBlock_destroy(DiskDriver* disk, DirectoryDataBlock* ddb)
{
    for(int i = 0; i < FILE_BLOCK_SZ; ++i)
    {
        FirstFileBlock ffb;
        if(ddb->file_blocks[i] == BLOCKCOMMON_FREE)
            continue;
        DiskDriver_readBlock(disk, &ffb, ddb->file_blocks[i]);
        if(ffb.fcb.is_dir)
        {
            FirstDirectoryBlock* fdb = (FirstDirectoryBlock*) &ffb; 
            DirectoryBlock_destroy(disk, fdb);
        }
        else
        {
            FileBlock_destroy(disk, &ffb);
        }
        DiskDriver_freeBlock(disk, ddb->file_blocks[i]);   
    }

    return 0;
}

int DirectoryBlock_destroy(DiskDriver* disk, FirstDirectoryBlock* fdb)
{
    for(int i = 0; i < DIRECT_BLOCK_SZ; ++i)
    {
        DirectoryDataBlock ddb;

        if(fdb->direct_blocks[i] == BLOCKCOMMON_FREE)
            continue;

        DiskDriver_readBlock(disk, &ddb, fdb->direct_blocks[i]);
        DirectoryDataBlock_destroy(disk, &ddb);
        DiskDriver_freeBlock(disk, fdb->direct_blocks[i]);
    }
    for(int i = 0; i < INDIRECT_BLOCK_SZ; ++i)
    {
        IndexBlock ib;

        if(fdb->indirect_blocks[i] == BLOCKCOMMON_FREE)
            continue;

        DiskDriver_readBlock(disk, &ib, fdb->indirect_blocks[i]);
        for(int j = 0; j < INDEX_BLOCK_SZ; ++j)
        {
            DirectoryDataBlock ddb;

            if(ib.next_blocks[j] == BLOCKCOMMON_FREE)
                continue;

            DiskDriver_readBlock(disk, &ddb, ib.next_blocks[j]);
            DirectoryDataBlock_destroy(disk, &ddb);
            DiskDriver_freeBlock(disk, ib.next_blocks[j]);
        }
        DiskDriver_freeBlock(disk, fdb->indirect_blocks[i]);
    }
    DiskDriver_freeBlock(disk, fdb->fcb.block_in_disk);

    return 0;
}
