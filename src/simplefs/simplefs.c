#include "simplefs.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


//

DirectoryHandle* DirectoryHandle_init(SimpleFS* fs, FirstDirectoryBlock* fdb, DirectoryDataBlock* ddb)
{
    DirectoryHandle* dir_handle = malloc(sizeof(DirectoryHandle));
    dir_handle->sfs = fs;
    DirectoryBlockHandle_init(&(dir_handle->block_handle), fdb, ddb);
    
    return dir_handle;
}

FileHandle* FileHandle_init(DirectoryHandle* d, FirstFileBlock* ffb, FileDataBlock* fdb)
{
    FileHandle* fh = malloc(sizeof(FileHandle));
    fh->sfs = d->sfs;
    FileBlockHandle_init(&(fh->block_handle), ffb, fdb);

    return fh;
}

//

DirectoryHandle* SimpleFS_init(SimpleFS* fs, DiskDriver* disk)
{
    fs->disk = disk;
    
    char name[] = "/";
    FirstDirectoryBlock_init(fs->disk, name, -1);
    
    FirstDirectoryBlock* fdb = (FirstDirectoryBlock*) DiskDriver_getBlock(fs->disk, 0);
    DirectoryDataBlock* ddb = (DirectoryDataBlock*) DiskDriver_getBlock(fs->disk, fdb->direct_blocks[0]);

    DirectoryHandle* dh = DirectoryHandle_init(fs, fdb, ddb);
    fs->current_dir[0] = '\0';

    return dh;
}

DirectoryHandle* SimpleFS_format(SimpleFS* fs)
{

    int block_num = DiskDriver_getNumBlocks(fs->disk);
    for(int i = 0; i < block_num; ++i)
    {
        DiskDriver_freeBlock(fs->disk, i);
    }

    char name[] = "/";
    FirstDirectoryBlock_init(fs->disk, name, -1);

    FirstDirectoryBlock* fdb = (FirstDirectoryBlock*) DiskDriver_getBlock(fs->disk, 0);
    DirectoryDataBlock* ddb = (DirectoryDataBlock*) DiskDriver_getBlock(fs->disk, fdb->direct_blocks[0]);
    fs->current_dir[0] = '\0';

    DirectoryHandle* dh = DirectoryHandle_init(fs, fdb, ddb);

    return dh;
}

FileHandle* SimpleFS_createFile(DirectoryHandle* d, const char* filename)
{
    int dir_data_block = d->block_handle.current_block->block_in_file;
    assert(d->block_handle.current_block->is_data_block == 1);

    if(DirectoryBlock_searchFile(d->sfs->disk, &(d->block_handle), filename, 0) != -1)
    {
        printf("SimpleFS_createFile() file %s already in directory\n", filename);
        return (FileHandle*)-1;
    }

    int block_file = FirstFileBlock_init(d->sfs->disk, filename, dir_data_block);

    if(block_file == -1) 
    {
        return (FileHandle*)-1;
    }

    DirectoryBlock_addFile(d->sfs->disk, &(d->block_handle), block_file);
    
    FirstFileBlock* pffb = (FirstFileBlock*) DiskDriver_getBlock(d->sfs->disk, block_file);
    FileDataBlock* fdb = (FileDataBlock*) DiskDriver_getBlock(d->sfs->disk, pffb->direct_blocks[0]);
    FileHandle* fh = FileHandle_init(d, pffb, fdb);

    return fh;
}

int SimpleFS_readDir(char** names, DirectoryHandle* d)
{
    FirstDirectoryBlock fdb;
    DiskDriver_readBlock(d->sfs->disk, &fdb, d->block_handle.dcb->fcb.block_in_disk);

    int nelements = 0;
    for(int i = 0; i < d->block_handle.dcb->num_entries; ++i)
    {
        int file_block = DirectoryBlock_getFile(d->sfs->disk,&fdb,i);
        if(file_block == -1)
        {
            continue;
        }
        FirstFileBlock* pffb = (FirstFileBlock*) DiskDriver_getBlock(d->sfs->disk, file_block);
        
        strcpy(names[nelements++], pffb->fcb.name);
    }

    return nelements;
}

int SimpleFS_close(void* f)
{
    free(f);

    return 0;
}

int SimpleFS_write(FileHandle* f, void* data, int size)
{
    return FileBlock_write(f->sfs->disk, &(f->block_handle), data, size);
}

int SimpleFS_read(FileHandle* f, void* data, int size)
{
    return FileBlock_read(f->sfs->disk, &(f->block_handle), data, size);    
}

int SimpleFS_seek(FileHandle* f, int pos)
{
    return FileBlock_seek(f->sfs->disk, &f->block_handle, pos);
}

DirectoryHandle* SimpleFS_mkDir(DirectoryHandle* dhparent, char* dirname)
{
    DiskDriver* disk = dhparent->sfs->disk;
    DirectoryBlockHandle* dbh = &(dhparent->block_handle);

    if(DirectoryBlock_searchFile(disk, dbh, dirname, 0) != -1)
    {
        printf("SimpleFS_mkDir() file %s already in directory\n", dirname);
        return (DirectoryHandle*) -1;
    }
    
    int block_in_disk = FirstDirectoryBlock_init(disk, dirname, dbh->dcb->fcb.block_in_disk);

    DirectoryBlock_addFile(disk, dbh, block_in_disk);
    FirstDirectoryBlock* fdb = (FirstDirectoryBlock*) DiskDriver_getBlock(disk, block_in_disk);
    DirectoryDataBlock* ddb = (DirectoryDataBlock*) DiskDriver_getBlock(disk, fdb->direct_blocks[0]);
    return DirectoryHandle_init(dhparent->sfs, fdb, ddb);
}

int SimpleFS_remove(DirectoryHandle* dhparent, char* filename)
{
    DirectoryBlockHandle current_dbh = dhparent->block_handle;

    DirectoryBlock_removeFile(dhparent->sfs->disk, &current_dbh, filename);
    return 0;
}

void* SimpleFS_pathResolution(SimpleFS* fs, char* path)
{
    char* name = strtok(path, "/");

    FirstDirectoryBlock* fdb = (FirstDirectoryBlock*) DiskDriver_getBlock(fs->disk, 0);
    DirectoryDataBlock* ddb = (DirectoryDataBlock*) DiskDriver_getBlock(fs->disk, fdb->direct_blocks[0]);

    DirectoryHandle* dh = DirectoryHandle_init(fs, fdb, ddb);

    while(name != NULL)
    {
        int num_block = DirectoryBlock_searchFile(fs->disk, &(dh->block_handle), name, 0);
                
        name = strtok(NULL, "/");

        if(num_block == -1) 
            goto wrong_path;

        fdb = (FirstDirectoryBlock*) DiskDriver_getBlock(fs->disk, num_block);
        if(fdb->fcb.is_dir == 1)
        {
            SimpleFS_close(dh);
            ddb = (DirectoryDataBlock*) DiskDriver_getBlock(fs->disk, fdb->direct_blocks[0]);
            dh = DirectoryHandle_init(fs, fdb, ddb);
        }
        else
        {
            if(name != NULL) 
                goto wrong_path;

            FirstFileBlock* ffb = (FirstFileBlock*) DiskDriver_getBlock(fs->disk, num_block);
            FileDataBlock* fdb = (FileDataBlock*) DiskDriver_getBlock(fs->disk, ffb->direct_blocks[0]);
            FileHandle* fh = FileHandle_init(dh, ffb, fdb);
            SimpleFS_close(dh);
            return fh;
        }
        
    }
    return dh;

wrong_path:
    SimpleFS_close(dh);
    return (void*) -1;
}

static void SimpleFS_sanitizePath(char* dst, char* src)
{
    int slash = 0;

    if(*src == '\0')
    {
        *dst = '\0';
        return;
    } 

    while(*src != '\0')
    {
		if(*src == '/')
		{
			if(!slash)
			{
				slash = 1;
        		*dst++ = *src++; 		
			}
			else src++;
		}
		else
		{
			if(slash) slash = 0;
			*dst++ = *src++;
		}
    }
    if(*(dst-1) == '/') *(dst-1) = '\0';
    else *dst = '\0';
}

void SimpleFS_relativeToAbsolutePath(SimpleFS* fs, char* abs, char* rel)
{
    char raw_abs[SIMPLEFS_MAXLEN_PATH];

    if(rel[0] != '.')
    {
        strncpy(raw_abs, rel, SIMPLEFS_MAXLEN_PATH);
    } 
    else
    {
        strncpy(raw_abs, fs->current_dir, SIMPLEFS_MAXLEN_PATH);
        strncat(raw_abs, ++rel, SIMPLEFS_MAXLEN_PATH);   
    }
    SimpleFS_sanitizePath(abs, raw_abs);
}

int SimpleFS_extractLastComponent(char* last_component, char* path)
{
    char s[SIMPLEFS_MAXLEN_PATH];

    SimpleFS_sanitizePath(s, path);

    char* p = strrchr(s, '/'); 
    if(p == NULL)
    {
        return -1;
    }
    *p++ = '\0';
    strcpy(path, s);

    strncpy(last_component, p, SIMPLEFS_MAXLEN_PATH);

    return 0;
}