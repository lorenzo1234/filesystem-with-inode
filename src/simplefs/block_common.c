#include "block_common.h"

void BlockCommon_fillArray(int *array, int c, int len)
{
    for(int i = 0; i < len; i++)
        array[i] = c;
}

int IndexBlock_init(DiskDriver* disk, int free_block)
{
    IndexBlock ib = {
        .header.is_data_block = 0
    };
    ib.header.block_in_file = free_block;
    BlockCommon_fillArray(ib.next_blocks, BLOCKCOMMON_FREE, sizeof(ib.next_blocks)/sizeof(int));

    DiskDriver_writeBlock(disk, &ib, ib.header.block_in_file);

    return ib.header.block_in_file;
}

int BlockCommon_getFreeBlock(int array[], int size)
{
    for(int i = 0; i < size; ++i)
    {
        if(array[i] == BLOCKCOMMON_FREE)
            return i;
    }

    return -1;
}