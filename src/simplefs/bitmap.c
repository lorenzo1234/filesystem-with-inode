#include "bitmap.h"

#include <assert.h>
#include <stdio.h>

BitMapEntryKey BitMap_blockToIndex(int num)
{
    BitMapEntryKey e = {
        num / 8,
        num % 8
    };
    return e;
}

int BitMap_indexToBlock(int entry, uint8_t bit_num)
{
    return entry*8 + bit_num;
}

int BitMap_get(BitMap* bmap, int start, int status)
{
    if(start >= bmap->num_bits)
    {
        printf("BitMap_get(): attempt to access bit out of bitmap range\n");
        return -1;
    }

    BitMapEntryKey e = BitMap_blockToIndex(start);

    char mask = status ? 0xff : 0x00;

    char *entries = bmap->entries;
    for(int i = e.entry_num; i <= (bmap->num_bits/8); ++i)
    {
        // check if status bit in block
        if(~(entries[i] ^ mask))
        {
            char temp = entries[i];
            for(int j = 0; j <= 8; ++j)
            {
                if( ~(((temp>>j) ^ status)) & 1)
                {
                    int idx = BitMap_indexToBlock(i,j); 
                    if(idx >= start)
                    {
                        if(idx < bmap->num_bits)
                            return idx;
                        else
                            return -1;
                    }
                    else 
                        continue; 
                }

            }

        }

    }

    return -1;
}

int BitMap_set(BitMap* bmap, int pos, int status)
{
    if(pos >= bmap->num_bits)
    {
        printf("BitMap_set(): attempt to access bit out of bitmap range\n");
        return -1;
    }

    char *entries = bmap->entries;
    
    BitMapEntryKey e = BitMap_blockToIndex(pos);
    
    uint8_t upper_mask = (0xff << (e.bit_num+1));
    uint8_t bottom_mask = (0xff >> (8-e.bit_num));
    
    entries[e.entry_num] =  (entries[e.entry_num] & upper_mask) |
                            (status << e.bit_num) |
                            (entries[e.entry_num] & bottom_mask);
    
    return 0;
}
