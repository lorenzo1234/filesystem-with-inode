#include "disk_driver.h"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <sys/mman.h>

#include <assert.h>

#include <errno.h>

#define CHECK_ERR(RES, STR) \
    if(RES == -1) { \
        perror("Error " #STR); \
        exit(EXIT_FAILURE); \
    }

#define CHECK_ERR_P(RES, STR) \
    if(RES == (void*)-1) { \
        perror("Error " #STR); \
        exit(EXIT_FAILURE); \
    }

#define ALIGN_SZ(SZ, ALIGN) \
    ( ( (SZ) + ALIGN-1 ) & ~(ALIGN-1) )

int DiskDriver_init(DiskDriver* disk, const char* filename, int num_blocks)
{

    if(strnlen(filename, DISKDRIVER_MAX_FILENAME_LEN) >= DISKDRIVER_MAX_FILENAME_LEN)
    {
        exit(EXIT_FAILURE);
    }
    strcpy(disk->filename, filename);

    int real_nblocks = num_blocks+1;
    long total_size = BLOCK_SIZE*real_nblocks; 
    int real_bitmap_sz = ALIGN_SZ(num_blocks, 8) / 8; // number of blocks must be divisible by 8 (one byte)
    
    if(real_bitmap_sz+sizeof(DiskHeader) > BLOCK_SIZE)
    {
        printf("DiskDriver_init(): too many blocks, bitmap must fit in one block\n");
        exit(EXIT_FAILURE);
    }

    int errsv = 0;
    
    disk->fd = open(filename, O_RDWR, 0600);
    if(disk->fd == -1)
    {
        if(errno == ENOENT)
        {
            errsv = errno;
            disk->fd = open(filename, O_RDWR | O_CREAT, 0600);
        }
    }    

    int result = ftruncate(disk->fd, total_size);
    CHECK_ERR(result, "DiskDriver_init(): ftruncate()")

    disk->header = (DiskHeader*) mmap(NULL,
				   total_size,
				   PROT_READ|PROT_WRITE,
				   MAP_SHARED,
				   disk->fd,
				   0);
    CHECK_ERR_P(disk->header, "DiskDriver_init(): mmap()")

    disk->bitmap_data = ((char*)disk->header) + sizeof(DiskDriver);

    int current_num_blocks = num_blocks;
    if(errsv == ENOENT)
    {
        disk->header->num_blocks = real_nblocks;
        disk->header->bitmap_blocks = num_blocks;
        disk->header->bitmap_entries = real_bitmap_sz;
        disk->header->free_blocks = num_blocks;
        disk->header->first_free_block = 0;
        memset(disk->bitmap_data, DISKDRIVER_FREE, real_bitmap_sz);
    }
    else
    {
        current_num_blocks = num_blocks;
    }
    
    return current_num_blocks;
}

int DiskDriver_readBlock(DiskDriver* disk, void* dest, int block_num)
{
    if(block_num < 0)
    {
        printf("DiskDriver_readBlock(): block index must be greater than 0\n");
        return -1;
    }

    BitMap bmap;
    bmap.num_bits = disk->header->bitmap_blocks;
    bmap.entries = disk->bitmap_data;

    int idx = BitMap_get(&bmap, block_num, DISKDRIVER_INUSE);
    if(idx != block_num)
    {
        printf("DiskDriver_readBlock(): can't read a free block\n");
        return -1;
    }


    char* blocks = (char*) disk->header+BLOCK_SIZE;
    memcpy((char*)dest, blocks+block_num*BLOCK_SIZE, BLOCK_SIZE);
    return 0;           
}

int DiskDriver_writeBlock(DiskDriver* disk, void* src, int block_num)
{
    if(block_num < 0)
    {
        printf("DiskDriver_writeBlock(): block index must be greater than 0\n");
        return -1;
    }

    BitMap bmap;
    bmap.num_bits = disk->header->bitmap_blocks;
    bmap.entries = disk->bitmap_data;

    int res = BitMap_get(&bmap, block_num, DISKDRIVER_INUSE);
    if(res != block_num) // not already in use
    {
        res = BitMap_set(&bmap, block_num, DISKDRIVER_INUSE);
        if(res == -1)
        {
            return -1;
        }
        disk->header->free_blocks--;
    }


    if(block_num == disk->header->first_free_block)
    {
        disk->header->first_free_block = BitMap_get(&bmap, 0, DISKDRIVER_FREE);
    }

    char* blocks = (char*) disk->header+BLOCK_SIZE;
    memcpy(blocks+block_num*BLOCK_SIZE, (char*)src, BLOCK_SIZE);
    
    assert(disk->header->free_blocks >= 0);
    
    return 0;
}

int DiskDriver_freeBlock(DiskDriver* disk, int block_num)
{
    BitMap bmap;
    bmap.num_bits = disk->header->bitmap_blocks;
    bmap.entries = disk->bitmap_data;

    int res = BitMap_get(&bmap, block_num, DISKDRIVER_FREE);
    
    if(res == block_num)
    {
        // printf("DiskDriver_freeBlock(): block %d already freed\n", block_num);
        return -1;
    }

    res = BitMap_set(&bmap, block_num, DISKDRIVER_FREE);
    if(res != -1)
    {
        int first_free_block = disk->header->first_free_block; 
        if(block_num < first_free_block || first_free_block == -1)
        {
            disk->header->first_free_block = BitMap_get(&bmap, 0, DISKDRIVER_FREE);
        }

        disk->header->free_blocks++;
        assert(disk->header->free_blocks <= disk->header->bitmap_blocks);
    }
    
    return res;
}

int DiskDriver_getFreeBlock(DiskDriver* disk, int start)
{
    BitMap bmap;
    bmap.num_bits = disk->header->bitmap_blocks;
    bmap.entries = disk->bitmap_data;

    assert(disk->header->free_blocks >= 0);
    if(disk->header->free_blocks == 0)
    {
        printf("DiskDriver_getFreeBlock(): no free_block available\n");
        return -1;
    }
    
    return BitMap_get(&bmap, start, DISKDRIVER_FREE);
}

int DiskDriver_flush(DiskDriver* disk)
{
    int num_blocks = disk->header->num_blocks;

    int res = msync(disk->header, BLOCK_SIZE*num_blocks, MS_SYNC);
    CHECK_ERR(res, "DiskDriver_flush(): msync(disk->header)")
       
    return 0;
}

void DiskDriver_free(DiskDriver* disk)
{   
    int num_blocks = disk->header->num_blocks;

    int res = munmap(disk->header, BLOCK_SIZE*num_blocks);
    CHECK_ERR(res, "DiskDriver_free(): munmap(disk->header)")
    res = close(disk->fd);
    CHECK_ERR(res, "DiskDriver_free(): close(disk->fd)");
}

void DiskDriver_destroy(DiskDriver *disk)
{
    int res = unlink(disk->filename);
    CHECK_ERR(res, "DiskDriver_desroy(): unlink()");
}

char* DiskDriver_getBlock(DiskDriver* disk, int block_num)
{
    if(block_num >= disk->header->bitmap_blocks)
        return (char*)-1;
    
    char* block = (char*) disk->header+BLOCK_SIZE;

    return block+BLOCK_SIZE*block_num;
}

int DiskDriver_getFirstFreeBlock(DiskDriver* disk)
{
    assert(disk->header->free_blocks >= 0);
    if(disk->header->free_blocks == 0)
    {
        printf("DiskDriver_getFirstFreeBlock(): no free_block available\n");
        return -1;
    }
    

    return disk->header->first_free_block;
}

int DiskDriver_getNumBlocks(DiskDriver* disk)
{
    return disk->header->bitmap_blocks;
}