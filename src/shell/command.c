#include "command.h"
#include "simplefs.h"
#include "linenoise.h"

#include "runtime_manager.h"

#include <stdlib.h>
#include <stdio.h>

#define NUM_COMMANDS 17

static Command commands[NUM_COMMANDS];

int Command_SimpleFS_init(void)
{
    CommandsInit();

    RunTimeManager_init();

    return 0;
}

static int Command_mount(Command* command)
{
    char* moutning_point = command->args[0];
    char* diskname = command->args[1];

    return RunTimeManager_mount(moutning_point, diskname, RUNTIMEMANAGER_NBLOCKS);
}

static int Command_unmount(Command* command)
{
    char* mounting_point = command->args[0];

    RunTimeManager_unmount(mounting_point);

    return 0;
}

static int Command_listDisk(Command* command)
{

    int n = RUNTIMEMANAGER_MAX_MOUNT_FS;
    char** names = malloc(n*sizeof(char*));
    for(int i =0 ; i < n; ++i)
    {
        names[i] = malloc(MAX_NAME_LEN);
    }

    int m = RunTimeManager_listDisk(names);
    printf("\t");
    for(int i = 0; i < m; ++i)
        printf("%s\t", names[i]);
    printf("\n");

    for(int i = 0; i < n; ++i)
    {
        free(names[i]);
    }
    free(names);

    return 0;
}

static int Command_format(Command* command)
{
    char* diskname = command->args[0];

    return RunTimeManager_format(diskname);
}

static int Command_createFile(Command* command)
{
    char* path = command->args[0];
    char path_copy[SIMPLEFS_MAXLEN_PATH];

    strncpy(path_copy, path, SIMPLEFS_MAXLEN_PATH);

    char filename[SIMPLEFS_MAXLEN_PATH];
    SimpleFS_extractLastComponent(filename, path);

    DirectoryHandle* dh = RunTimeManager_pathResolution(path);

    if(dh == (DirectoryHandle*) -1)
    {
        return -1;
    }

    FileHandle* fh = SimpleFS_createFile(dh, filename);
    RunTimeManager_addFileHandle(fh, path_copy);
    
    return 0;
}

static int Command_readDir(Command* command)
{
    char* dirname = command->args[0];

    DirectoryHandle* dh = RunTimeManager_pathResolution(dirname);
    if(dh == (DirectoryHandle*) -1)
    {
        return -1;
    }
    int n = dh->block_handle.dcb->num_entries;

    char** names = malloc(n*sizeof(char*));
    for(int i =0 ; i < n; ++i)
    {
        names[i] = malloc(MAX_NAME_LEN);
    }

    int m = SimpleFS_readDir(names, dh);
    printf("\t");
    for(int i = 0; i < m; ++i)
        printf("%s\t", names[i]);
    printf("\n");

    for(int i = 0; i < n; ++i)
    {
        free(names[i]);
    }
    free(names);

    return 0;
}

static int Command_openFile(Command* command)
{
    char* path = command->args[0];
    char path_copy[SIMPLEFS_MAXLEN_PATH];

    strncpy(path_copy, path, SIMPLEFS_MAXLEN_PATH);

    FileHandle* fh = RunTimeManager_pathResolution(path);

    if(fh == (FileHandle*) -1 || fh->block_handle.fcb->fcb.is_dir)
    {
        return -1;
    }

    RunTimeManager_addFileHandle(fh, path_copy);
    
    return 0;
}

static int Command_close(Command* command)
{
    char* filename = command->args[0];

    FileHandle* fh = RunTimeManager_getFileHandleByPath(filename);
    if(fh == (FileHandle*)-1)
    {
        return -1;
    }
    int res = SimpleFS_close(fh);
    RunTimeManager_removeFileHandle(fh);

    return res;
}

#define MAX_INPUT_DATA 500

static int Command_write(Command* command)
{
    char* filename = command->args[0];
     
    char data[MAX_INPUT_DATA];
    int len = 0;
    char c;
    do
    {
        c = getchar();
        data[len++] = c;
    } while (c != '\n' && len < MAX_INPUT_DATA);    
    data[len-1] = '\0';

    FileHandle* fh = RunTimeManager_getFileHandleByPath(filename);
    if(fh == (FileHandle*)-1)
    {
        return -1;
    }
    int res = SimpleFS_write(fh, &data, len);
    
    res = len == res ? 0 : -1;
    
    return res;
}

static int Command_read(Command* command)
{
    char* filename = command->args[0];
    int size = atoi(command->args[1]);

    if(size >= MAX_INPUT_DATA)
    {
        printf("size must be less than %d characters\n", MAX_INPUT_DATA);
        return -1;
    }

    if(size <= 0)
    {
        printf("size must greater than 0\n");
        return -1;
    }

    char data[MAX_INPUT_DATA];
    FileHandle* fh = RunTimeManager_getFileHandleByPath(filename);
    if(fh == (FileHandle*)-1)
    {
        return -1;
    }
    int ret = SimpleFS_read(fh, &data, size);
    data[size-1] = '\0';

    printf("%s\n", data);

    ret = ret == size ? 0 : -1;

    return ret;
}

static int Command_seek(Command* command)
{
    char* filename = command->args[0];
    int pos = atoi(command->args[1]);

    FileHandle* fh = RunTimeManager_getFileHandleByPath(filename);
    if(fh == (FileHandle*)-1)
    {
        return -1;
    }
    return SimpleFS_seek(fh, pos);
}

static int Command_changeDir(Command* command)
{
    char* path = command->args[0];

    int ret = RunTimeManager_changeDir(path);

    return ret;    
}

static int Command_mkDir(Command* command)
{

    char* path = command->args[0];
    char path_copy[SIMPLEFS_MAXLEN_PATH];

    strncpy(path_copy, path, SIMPLEFS_MAXLEN_PATH);

    char dirname[SIMPLEFS_MAXLEN_PATH];
    SimpleFS_extractLastComponent(dirname, path);

    DirectoryHandle* dhparent = RunTimeManager_pathResolution(path);

    if(dhparent == (DirectoryHandle*) -1)
    {
        return -1;
    }

    DirectoryHandle* dh = SimpleFS_mkDir(dhparent, dirname);
    if(RunTimeManager_getDirHandleByPath(path_copy) == (DirectoryHandle*) -1)
    {
        RunTimeManager_addDirHandle(dh, path_copy);
    }
    else
    {
        SimpleFS_close(dh);
    }
    

    return 0;
}

static int Command_remove(Command* command)
{
    char* path = command->args[0];
    char name[SIMPLEFS_MAXLEN_PATH];

    FileHandle* h = RunTimeManager_pathResolution(path);
    if(h == (FileHandle*) -1)
    {
        return -1;
    }
    
    SimpleFS_extractLastComponent(name, path);
    
    DirectoryHandle* dhparent = RunTimeManager_pathResolution(path);
    if(dhparent == (DirectoryHandle*) -1)
    {
        return -1;
    }

    int ret = SimpleFS_remove(dhparent, name);

    RunTimeManager_removeFileHandle(h);
    RunTimeManager_resetDirCache();

    return ret;
}

static int Command_exit(Command* command)
{
    RunTimeManager_resetOpenFileTable();
    RunTimeManager_exit();

    return 0;
}

static int Command_help(Command* command)
{
    printf(
        "\t* format <diskname> \n"\
        "\t* create <path>\n"\
        "\t* ls <dir>\n"\
        "\t* open <path>\n"\
        "\t* close <file>\n"\
        "\t* write <file>\n"\
        "\t* read <read> <size>\n"\
        "\t* seek <file> <cursor>\n"\
        "\t* cd <dir>\n"\
        "\t* mkdir <dir>\n"\
        "\t* rm <file/dir>\n"\
        "\t* mount <mounting point> <disk name>\n"\
        "\t* unmount <mounting point>\n"\
        "\t* lsdir\n"\
        "\t* exit\n"\
        "\t* help\n"\
        "\n"\
        "\tAny file needs to be opened before used, and closed before removed.\n"\
        "\tThe Directory cache and the open file table will reset every time format, mount or unmount is executed.\n"\
        );
    
    return 0;   
}

static int Command_error(Command* command)
{
    printf("command not found\n");
    return -1;
}

void completionCallback(const char *buf, linenoiseCompletions *lc) 
{
    for(int i=0; i<NUM_COMMANDS; i++)
    {
        if(!strncmp(buf, commands[i].command_name, strlen(buf)))
        {
            linenoiseAddCompletion(lc, commands[i].command_name);    
        }
    }
}

int CommandInit(Command* command, char* command_name, int num_arguments, int (*execute)(Command*))
{
    if(strlen(command_name) > MAX_COMMAND_LEN)
    {
        return -1;
    }

    strcpy(command->command_name, command_name);
    command->argc = num_arguments;
    command->execute = execute;

    return 0;
}

int CommandsInit(void)
{
    CommandInit(&commands[0], "format", 1, Command_format);
    CommandInit(&commands[1], "create", 1, Command_createFile);
    CommandInit(&commands[2], "ls", 1, Command_readDir);
    CommandInit(&commands[3], "open", 1, Command_openFile);
    CommandInit(&commands[4], "close", 1, Command_close);
    CommandInit(&commands[5], "write", 1, Command_write);
    CommandInit(&commands[6], "read", 2, Command_read);
    CommandInit(&commands[7], "seek", 2, Command_seek);
    CommandInit(&commands[8], "cd", 1, Command_changeDir);
    CommandInit(&commands[9], "mkdir", 1, Command_mkDir);
    CommandInit(&commands[10], "rm", 1, Command_remove);
    CommandInit(&commands[11], "mount", 2, Command_mount);
    CommandInit(&commands[12], "unmount", 1, Command_unmount);
    CommandInit(&commands[13], "lsdisk", 0, Command_listDisk);
    CommandInit(&commands[14], "exit", 0, Command_exit);
    CommandInit(&commands[15], "help", 0, Command_help);
    CommandInit(&commands[NUM_COMMANDS-1], "error", 0, Command_error);
 
    linenoiseSetCompletionCallback(completionCallback);

    return 0;
}

Command getCommand(char* command_name)
{
    for(int i = 0; i < NUM_COMMANDS; ++i)
    {
        if(strcmp(command_name, commands[i].command_name) == 0)
        {
            return commands[i];
        }
    }

    return commands[NUM_COMMANDS-1];    
}