#include "runtime_manager.h"

#define MAX_NFILEHANDLE 3
#define MAX_NDIRHANDLE 10

#define MAXLEN_DISKNAME 20

#define FREE_HANDLE (void*) -1

int actual_num_blocks = RUNTIMEMANAGER_NBLOCKS;

static DiskDriver disk;
static SimpleFS fs;

typedef struct {
    FileHandle* handle[MAX_NFILEHANDLE];
    char path[MAX_NFILEHANDLE][SIMPLEFS_MAXLEN_PATH];
    int num_handle;
} OpenFileTable;

typedef struct {
    char mp[SIMPLEFS_MAXLEN_PATH];
    SimpleFS fs;  
    DiskDriver disk;
    int len;
    char v;
} MountTableEntry;

typedef struct {
    MountTableEntry entry[RUNTIMEMANAGER_MAX_MOUNT_FS];
    int num_fs;
} MountTable;

typedef struct {
    DirectoryHandle* handle[MAX_NDIRHANDLE];
    char path[MAX_NDIRHANDLE][SIMPLEFS_MAXLEN_PATH];
    char v[MAX_NDIRHANDLE];
    int num_handle;
} DirectoryCache;

static OpenFileTable oft;
static MountTable mt;
static DirectoryCache dc;

static int running = 0;

void print_open_file_table(void)
{
    printf("printing OpenFileTable\n");
    for(int i = 0; i < MAX_NFILEHANDLE; ++i)
    {
        if(oft.handle[i] != FREE_HANDLE)
        {
            printf("\t%d: %s\n",i, oft.path[i]);
        }
    }
}
void print_mount_table(void)
{
    printf("printing MountTable\n");
    for(int i = 0; i < RUNTIMEMANAGER_MAX_MOUNT_FS; ++i)
    {
        if(mt.entry[i].v)
        {
            printf("\t%d: %s\n",i, mt.entry[i].mp);
        }
    }
}
void print_dir_cache(void)
{
    printf("printing DirectoryCache\n");
    for(int i = 0; i < MAX_NDIRHANDLE; ++i)
    {
        if(dc.v[i])
        {
            printf("\t%d: %s\n",i, dc.path[i]);
        }
    }
}


static int getFreeHandle(void)
{
    for(int i = 0; i < MAX_NFILEHANDLE; ++i)
        if(oft.handle[i] == FREE_HANDLE)
            return i;
    return -1;
}

static int getFreeMountTableEntry(void)
{
    for(int i = 0; i < RUNTIMEMANAGER_MAX_MOUNT_FS; ++i)
        if(!mt.entry[i].v)
            return i;
    return -1;
}

int RunTimeManager_init()
{
    for(int i = 0; i < MAX_NFILEHANDLE; i++)
        oft.handle[i] = FREE_HANDLE;
    oft.num_handle = 0;


    for(int i = 0; i <  RUNTIMEMANAGER_MAX_MOUNT_FS; i++)
        mt.entry[i].v = 0;
    mt.num_fs = 0;

    for(int i = 0; i < MAX_NDIRHANDLE; i++)
        dc.v[i] = 0;
    dc.num_handle = 0;
    
    char *pathname = "root";
    actual_num_blocks = DiskDriver_init(&disk, pathname, RUNTIMEMANAGER_NBLOCKS);
    DirectoryHandle* dh = SimpleFS_init(&fs, &disk);
    RunTimeManager_addDirHandle(dh, "/");

    running = 1;

    return 0;
}

int RunTimeManager_format(char* diskname)
{
    DirectoryHandle* dh;

    RunTimeManager_resetDirCache();
    RunTimeManager_resetOpenFileTable();

    for(int i = 0; i < RUNTIMEMANAGER_MAX_MOUNT_FS; i++)
    {
        if(mt.entry[i].v &&
        strncmp(mt.entry[i].disk.filename, diskname, MAXLEN_DISKNAME) == 0)
        {
            dh = SimpleFS_format( &(mt.entry[i].fs) );
            SimpleFS_close(dh);
            return 0;
        }
    }

    if(strncmp(disk.filename, diskname, MAXLEN_DISKNAME) == 0)
    {
        dh = SimpleFS_format(&fs);
        if(dh == (DirectoryHandle*) -1)
        {
            return -1;
        }
        RunTimeManager_resetOpenFileTable();
        RunTimeManager_addDirHandle(dh, "/");
    }

    return 0;
}

int RunTimeManager_changeDir(char* path) 
{
    char abs[SIMPLEFS_MAXLEN_PATH];
    SimpleFS_relativeToAbsolutePath(&fs, abs, path);
    
    DirectoryHandle* dh = RunTimeManager_pathResolution(path);
    if(dh == (DirectoryHandle*) -1)
    {
        return -1;
    }
    strncpy(fs.current_dir, abs, 20);

    return 0;
}

void* RunTimeManager_pathResolution(char* path)
{
    char path_copy[SIMPLEFS_MAXLEN_PATH], 
    abs[SIMPLEFS_MAXLEN_PATH];
    DirectoryHandle* dh;

    SimpleFS_relativeToAbsolutePath(&fs, abs, path);

    dh = RunTimeManager_getDirHandleByPath(abs);
    if(dh == (DirectoryHandle*) -1)
    {    
        strncpy(path_copy, abs, SIMPLEFS_MAXLEN_PATH);
        for(int i = 0; i < RUNTIMEMANAGER_MAX_MOUNT_FS; i++)
        {
            if( mt.entry[i].v &&
             strncmp(path_copy, mt.entry[i].mp, mt.entry[i].len) == 0)
            {
                dh = SimpleFS_pathResolution(&mt.entry[i].fs, path_copy+mt.entry[i].len);
                RunTimeManager_addDirHandle(dh, abs);
                return dh;
            }
        }

        dh = SimpleFS_pathResolution(&fs, abs);
        RunTimeManager_addDirHandle(dh, path_copy);     
    }

    return dh;
}

int RunTimeManager_mount(char* mounting_point, char* diskname, int num_blocks)
{
    char abs[SIMPLEFS_MAXLEN_PATH];
    SimpleFS_relativeToAbsolutePath(&fs, abs, mounting_point);

    DirectoryHandle* dhmp = RunTimeManager_pathResolution(mounting_point);

    if(dhmp == (DirectoryHandle*)-1)
    {
        return -1;
    }

    int idx = getFreeMountTableEntry();

    if(idx == -1)
    {
        return -1;
    }

    actual_num_blocks = DiskDriver_init(&(mt.entry[idx].disk), diskname, num_blocks);
    DirectoryHandle* dhroot = SimpleFS_init( &(mt.entry[idx].fs), &(mt.entry[idx].disk) );
    strncpy(mt.entry[idx].mp, abs, SIMPLEFS_MAXLEN_PATH);
    mt.entry[idx].len = strlen(abs);
    mt.entry[idx].v = 1;

    mt.num_fs++;

    SimpleFS_close(dhroot);

    RunTimeManager_resetDirCache();
    RunTimeManager_resetOpenFileTable();

    return 0;
}

int RunTimeManager_unmount(char* mounting_point)
{
    char abs[SIMPLEFS_MAXLEN_PATH];
    SimpleFS_relativeToAbsolutePath(&fs, abs, mounting_point);

    for(int i = 0; i < RUNTIMEMANAGER_MAX_MOUNT_FS; i++)
    {
        if(mt.entry[i].v && 
        strncmp(abs, mt.entry[i].mp, mt.entry[i].len) == 0 )
        {
            DiskDriver_free(&(mt.entry[i].disk));
            mt.entry[i].v = 0;
            mt.num_fs--;
            RunTimeManager_resetDirCache();
            RunTimeManager_resetOpenFileTable();
            return 0;
        }
    }

    return -1;
}

void RunTimeManager_resetMountTable(void)
{
    for(int i = 0; i < RUNTIMEMANAGER_MAX_MOUNT_FS; i++)
    {
        if(mt.entry[i].v)
        {
            DiskDriver_free(&(mt.entry[i].disk));
            mt.entry[i].v = 0;
        }
    }
    mt.num_fs = 0;
}

int RunTimeManager_addFileHandle(FileHandle* h, char* path)
{

    if(strlen(path) >= SIMPLEFS_MAXLEN_PATH)
    {
        return -1;
    }


    int idx = getFreeHandle();    
    if(idx == -1)
    {
        SimpleFS_close(h);
        printf("RunTimeManager: too many open files, close or remove some files\n");
        return -1;
    }

    char abs[SIMPLEFS_MAXLEN_PATH];
    SimpleFS_relativeToAbsolutePath(&fs, abs, path);

    if(RunTimeManager_getFileHandleByPath(abs) != (FileHandle*) -1)
    {
        return -1;
    }

    strncpy(oft.path[idx], abs, SIMPLEFS_MAXLEN_PATH);
    oft.handle[idx] = h;
    oft.num_handle++;

    return 0;
}

void RunTimeManager_removeFileHandle(FileHandle* h)
{
    for(int i = 0; i < MAX_NFILEHANDLE; ++i)
    {
        if(h == oft.handle[i])
        {
            oft.handle[i] = FREE_HANDLE;
        }
    }
    oft.num_handle--;
}

FileHandle* RunTimeManager_getFileHandleByPath(char* path)
{
    char abs[SIMPLEFS_MAXLEN_PATH];
    SimpleFS_relativeToAbsolutePath(&fs, abs, path);

    for(int i = 0; i < MAX_NFILEHANDLE; ++i)
    {
        FileHandle* h = oft.handle[i];
        if(h == FREE_HANDLE)
            continue;
        if(strcmp(oft.path[i], abs) == 0)
        {
            return h;
        }
    }

    return (FileHandle*) -1;
}

void RunTimeManager_resetOpenFileTable(void)
{
    for(int i = 0; i < MAX_NFILEHANDLE; ++i)
    {
        if(oft.handle[i] != FREE_HANDLE)
        {
            SimpleFS_close(oft.handle[i]);
            oft.handle[i] = FREE_HANDLE;
        }
    }
}

int RunTimeManager_addDirHandle(DirectoryHandle* dh, char* path)
{
    if(strlen(path) >= SIMPLEFS_MAXLEN_PATH)
    {
        return -1;
    }

    if(dh == (DirectoryHandle*) -1)
    {
        return -1;
    }

    if(!dh->block_handle.dcb->fcb.is_dir)
    {
        return -1;
    }

    char abs[SIMPLEFS_MAXLEN_PATH];
    SimpleFS_relativeToAbsolutePath(&fs, abs, path);

    if(dc.v[dc.num_handle]) 
        SimpleFS_close(dc.handle[dc.num_handle]);

    strncpy(dc.path[dc.num_handle], abs, SIMPLEFS_MAXLEN_PATH);
    dc.handle[dc.num_handle] = dh;
    dc.v[dc.num_handle] = 1;
    dc.num_handle = dc.num_handle == MAX_NDIRHANDLE-1 ? 0 : dc.num_handle + 1;

    return 0;
}

void RunTimeManager_removeDirHandleByPath(DirectoryHandle* dh)
{
    for(int i = 0; i < MAX_NDIRHANDLE; ++i)
    {
        if(dh == dc.handle[i])
        {
            SimpleFS_close(dc.handle[i]);
            dc.handle[i] = 0;
            dc.v[i] = 0;
            return;
        }
    }
    dc.num_handle--;   
}

DirectoryHandle* RunTimeManager_getDirHandleByPath(char* path)
{   

    for(int i = 0; i < MAX_NDIRHANDLE; ++i)
    {
        if(dc.v[i] &&
         strncmp(dc.path[i], path, SIMPLEFS_MAXLEN_PATH) == 0)
        {
            return dc.handle[i];
        }
    }

    return (DirectoryHandle*)-1;
}

void RunTimeManager_resetDirCache(void)
{
    for(int i = 0; i < MAX_NDIRHANDLE; ++i)
    {
        if(dc.v[i])
        {
            SimpleFS_close(dc.handle[i]);
            dc.handle[i] = 0;
            dc.v[i] = 0;
        }
    }
    dc.num_handle = 0;
}

int RunTimeManager_listDisk(char** names)
{   
    int nelements = 0;
    strncpy(names[nelements++], "root", MAXLEN_DISKNAME); 
    for(int i = 0; i < RUNTIMEMANAGER_MAX_MOUNT_FS; i++)
    {
        if(mt.entry[i].v)
        {
            strncpy(names[nelements++], mt.entry[i].disk.filename, MAXLEN_DISKNAME);
        }
    }

    return nelements;
}

SimpleFS* RunTimeManager_getSimpleFS(void)
{
    return &fs;
}

int RunTimeManager_running()
{
    return running;
}

void RunTimeManager_exit()
{
    DiskDriver_free(&disk);
    RunTimeManager_resetDirCache();
    running = 0;
}