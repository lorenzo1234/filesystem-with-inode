#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "command.h"
#include "runtime_manager.h"
#include "linenoise.h"

#define HISTORY_MAX_LEN 20

int main(void)
{
    printf(
    "Simple FileSystem\n"\
    "> type help for more info about commands.\n"\
    );

    Command_SimpleFS_init();

    linenoiseHistorySetMaxLen(HISTORY_MAX_LEN);

    char* line;

    while( RunTimeManager_running() && ((line = linenoise("> ")) != NULL) ) 
    {

        linenoiseHistoryAdd(line);

        char* command_name = strtok(line, " ");
        if(command_name == NULL)
            continue;

        Command command = getCommand(command_name);
        
        int i;
        for(i = 0; i < command.argc; ++i)
        {
            command.args[i] = strtok(NULL, " ");
            if(command.args[i] == NULL)
                break;
        }
        if(i == command.argc)
            command.execute(&command);
        else
            printf("command %s need %d arguments\n", command.command_name, command.argc);
        
        free(line);
        printf("\n##############################\n");
        print_open_file_table();
        print_mount_table();
        print_dir_cache();
        printf("##############################\n\n");
    }

    return 0;
}
