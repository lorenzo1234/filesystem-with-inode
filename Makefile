CCOPTS= -Wall -O3 -std=gnu99 -Wstrict-prototypes -Iinclude
LIBS= 
CC=gcc
AR=ar

INCDIR=include
OBJDIR=build
TESTDIR=tests
SRCDIR=src

BINS=simplefs_shell

SRC_SIMPLEFS_NO_BITMAP=$(filter-out $(SRCDIR)/simplefs/directory_block_no_bitmap.c, $(wildcard $(SRCDIR)/simplefs/*.c))
SRC_SIMPLEFS=$(filter-out $(SRCDIR)/simplefs/directory_block_no_bitmap.c, $(wildcard $(SRCDIR)/simplefs/*.c))
SRC_SHELL=$(wildcard $(SRCDIR)/shell/*.c)

OBJS_SIMPLEFS_NO_BITMAP=$(patsubst $(SRCDIR)/%,$(OBJDIR)/%,$(SRC_SIMPLEFS:.c=.o))
OBJS_SIMPLEFS=$(patsubst $(SRCDIR)/%,$(OBJDIR)/%,$(SRC_SIMPLEFS:.c=.o))
OBJS_SHELL=$(patsubst $(SRCDIR)/%,$(OBJDIR)/%,$(SRC_SHELL:.c=.o))

HEADERS=$(wildcard include/*.h)

DATAFILE=root

$(OBJDIR)/%.o:	$(SRCDIR)/%.c $(HEADERS)
	$(CC) $(CCOPTS) -c -o $@  $<

.phony: clean all

all:	$(BINS) 

$(BINS): $(OBJS_SIMPLEFS) $(OBJS_SHELL)
	$(CC) $(CCOPTS) -o $@ $^ $(LIBS)

simplefs_test: test/simplefs_test.c $(OBJS_SIMPLEFS) 
	$(CC) $(CCOPTS)  -o $@ $^ $(LIBS)

diskdriver_test: test/diskdriver_test.c $(OBJS_SIMPLEFS) 
	$(CC) $(CCOPTS)  -o $@ $^ $(LIBS)

bitmap_test: test/bitmap_test.c $(OBJS_SIMPLEFS) 
	$(CC) $(CCOPTS)  -o $@ $^ $(LIBS)

$(BINS)_no_bitmap: $(OBJS_SIMPLEFS_NO_BITMAP) $(OBJS_SHELL)
	$(CC) $(CCOPTS) -o $@ $^ $(LIBS)

simplefs_test_no_bitmap: test/simplefs_test.c $(OBJS_SIMPLEFS_NO_BITMAP) 
	$(CC) $(CCOPTS)  -o $@ $^ $(LIBS)

diskdriver_test_no_bitmap: test/diskdriver_test.c $(OBJS_SIMPLEFS_NO_BITMAP) 
	$(CC) $(CCOPTS)  -o $@ $^ $(LIBS)

bitmap_test_no_bitmap: test/bitmap_test.c $(OBJS_SIMPLEFS_NO_BITMAP) 
	$(CC) $(CCOPTS)  -o $@ $^ $(LIBS)

clean:
	rm -rf $(OBJDIR)/simplefs/*.o $(OBJDIR)/shell/*.o  $(BINS) $(BINS)_no_bitmap 
	rm -f $(DATAFILE)
