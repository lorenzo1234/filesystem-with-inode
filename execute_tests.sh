#!/bin/bash

TESTS_LIST=("bitmap_test" "diskdriver_test" "simplefs_test" "bitmap_test_no_bitmap" "diskdriver_test_no_bitmap" "simplefs_test_no_bitmap")

echo -ne "> Compiling tests\n\n"

for test in ${TESTS_LIST[@]}
do
	make $test
done

echo -ne "\n"


echo -ne "> Executing tests\n\n"

for test in ${TESTS_LIST[@]}
do

	echo -ne "* $test\n\n"
	./$test
	echo -ne "\n"

done

echo -ne "\n"

for test in ${TESTS_LIST[@]}
do
	rm $test
done
